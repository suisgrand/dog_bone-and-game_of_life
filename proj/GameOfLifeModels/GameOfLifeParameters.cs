using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace proj.GameOfLifeModels
{
    public class GameOfLifeParameters
    {
        public int Dimension { get; set; }
        public ICollection<CellGOL> Grid;

        public GameOfLifeParameters()
        {
            this.Grid = this.Grid = new Collection<CellGOL>();
        }


    }
}