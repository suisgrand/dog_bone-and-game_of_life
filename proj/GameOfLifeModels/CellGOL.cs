namespace proj.GameOfLifeModels
{
    public class CellGOL
    {
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsAlive { get; set; }
    }
}