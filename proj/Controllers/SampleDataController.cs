using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using proj.Dog2Bone;
using proj.Interfaces;

namespace proj.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {

        private readonly ILiveSettings _liveSettings;
        private readonly IDogMovement _dogMovement;
        public SampleDataController(ILiveSettings _ILiveSettings, IDogMovement mov)
        {
            _liveSettings = _ILiveSettings;
            _dogMovement = mov;
        }

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }
        [HttpGet("[action]")]
        public ClientSettings GetGrid()
        {
            var data = _liveSettings.GetData();
            var settings = _liveSettings.GetSettings();
            return settings;
        }

        [HttpPost("[action]")]
        public ClientSettings HandleUserPosition([FromBody] ClientSettings settings)
        {
            //System.Console.WriteLine("element: {0}", settings.Elements.Count);

            var result = _dogMovement.HandleDogMovement(settings);
            System.Console.WriteLine("elements");
            foreach (var item in settings.Elements)
            {
                System.Console.WriteLine("item: {0} -- x: {1} -- y: {2}",
                item.ElementObject, item.GridPosition.positionX, item.GridPosition.positionY);
            }

            return result;
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
