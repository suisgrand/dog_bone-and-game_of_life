using Microsoft.AspNetCore.Mvc;
using proj.GameOfLifeModels;
using proj.Interfaces.GameOfLife;

namespace proj.Controllers
{
    [Route("api/[controller]")]
    public class GameOfLifeController : Controller
    {

        private readonly IGameOfLifeSettings golSettings;

        public GameOfLifeController(IGameOfLifeSettings golSettings)
        {
            this.golSettings = golSettings;

        }

        [HttpGet("[action]")]
        public GameOfLifeParameters GetParameters()
        {
            return golSettings.GetDefaultParameters();
        }


    }
}