import { Position } from './GridSettings';

export interface ClientSettings {
    cellsX: number;
    cellsY: number;
    message: string;
    elements: GridPositionWithElement[];
}

export interface GridPositionWithElement {
    gridPosition: Position;
    elementObject: Element;
}


export enum Direction {
    north,
    south,
    west,
    east,
    none
}

export interface Position {
    positionX: number;
    positionY: number;
    direction: Direction;
}

export enum Element {
    dog,
    cat,
    bone,
    formerDog
}


