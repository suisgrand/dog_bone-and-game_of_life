import { element } from 'protractor';
import { FireServiceService } from './../service/fire-service.service';
import { Position, Element } from './../GridSettings';
import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClientSettings, Direction, GridPositionWithElement } from 'src/app/GridSettings';
import { GridCell, TextOrPicture } from '../model/GridCell';
import { runInThisContext } from 'vm';
import { createWiresService } from 'selenium-webdriver/firefox';
import { S_IFIFO } from 'constants';

@Component({
  selector: 'app-dog-home',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog-component.css']
})
export class DogComponent implements OnInit {

  settings: ClientSettings;
  CurrentDog: GridPositionWithElement = {
    gridPosition: {
      positionX: 0,
      positionY: 0,
      direction: Direction.none
    },
    elementObject: Element.dog
  };
  positionCorrectDog: Position = {
    positionX: 0,
    positionY: 0,
    direction: Direction.none
  };
  collectionX: number[] = [];
  collectionY: number[] = [];
  MyGrid: Grid[] = [];
  ready: Boolean = false;
  success = false;
  failure = false;

  imagePath = 'assets/dog.png';
  dogPath: 'assets/dog.png';

  myGridCell: GridCell =
    {
      ImageOrPicture: false,
      ImageName: ''
    };


  constructor(private http: HttpClient,
    private fireService: FireServiceService,
    @Inject('BASE_URL') private baseUrl: string) {

  }

  ngOnInit(): void {
    this.Initialize();
  }

  Initialize() {
    this.http.get<ClientSettings>(this.baseUrl + 'api/SampleData/GetGrid').subscribe(result => {
      this.MyGrid = [];
      this.collectionX = [];
      this.collectionY = [];

      this.CurrentDog = {
        gridPosition: {
          positionX: 0,
          positionY: 0,
          direction: Direction.none
        },
        elementObject: Element.dog
      };

      this.handleSettings(result);
      console.log(this.myGridCell);
      this.failure = false;
      this.success = false;

      this.fireService.display('success', 'Start Of The Game');

    }, error => {
      console.error(error);
    });
  }

  GetValue(x, y): GridCell {

    const val = this.MyGrid.find(item => item.X === x && item.Y === y);

    const resultGrid: GridCell = {
      ImageName: '',
      ImageOrPicture: false
    };

    if (val.item !== '') {
      resultGrid.ImageOrPicture = true;
      resultGrid.ImageName = val.item;
    } else {
      resultGrid.ImageOrPicture = false;
      resultGrid.ImageName = '';
    }

    return resultGrid;

  }

  UserClickCell(x: number, y: number) {

    if (this.success === true || this.failure === true) {
      return;
    }

    x--;
    y--;

    this.SendUserPositionToServer(x, y);
    // 4 situations:
    // user clicks on dog (rotate movement)
    // user clicks on dog or cat
    // user clicks on cell that will not get the dog to move
    // user clicks where the dog is set to move
  }



  SendUserPositionToServer(x: number, y: number) {

    console.log('x', y, 'ys', y, this.settings.elements);

    this.CurrentDog.elementObject = Element.formerDog;


    const newDogPosition: GridPositionWithElement = {
      gridPosition: {
        positionX: x,
        positionY: y,
        direction: this.CurrentDog.gridPosition.direction
      },
      elementObject: Element.dog
    };



    // // console.log('newDogPosition', newDogPosition);
    this.settings.elements.push(newDogPosition);

    // // console.log('settings before http post', this.settings.elements, this.CurrentDog.elementObject);
    // // for (const item of this.settings.elements) {
    // //   console.log('item', item.elementObject, new Date())
    // // }

    this.http.post<ClientSettings>(this.baseUrl + 'api/SampleData/HandleUserPosition',
      this.settings).subscribe(result => {

        if (result.message === 'Correct Movement') {
          this.settings.elements = [];
          this.MyGrid = [];
          this.collectionX = [];
          this.collectionY = [];
          this.handleSettings(result);
          this.fireService.display('success', result.message);
        } else if (result.message === 'Bone Was Found') {
          this.fireService.display('success', result.message);
          this.success = true;
        } else if (result.message === 'Dog Ran Into A Cat') {
          this.fireService.display('error', result.message);
          this.failure = true;
        } else {

          this.fireService.display('error', result.message);
          const formerDog = this.settings.elements.find(item => item.elementObject === Element.formerDog);
          this.removeDogs(Element.dog);

          // // console.log('result', result.elements, "settings", this.settings.elements,
          // //   "currentDog", this.CurrentDog);
        }
      }, error => {
        console.error(error);
      });

  }

  removeDogs(elementDog: Element) {
    const dog = this.settings.elements.find(item => item.elementObject === elementDog);
    if (dog) {
      const index = this.settings.elements.indexOf(dog, 0);
      if (index > -1) {
        this.settings.elements.splice(index, 1);
      }
    }
  }


  handleSettings(val: ClientSettings) {

    this.settings = val;
    this.collectionX = this.createCollection(this.settings.cellsX, this.collectionX);
    this.collectionY = this.createCollection(this.settings.cellsY, this.collectionY);

    this.CurrentDog = this.settings.elements.find(item => item.elementObject === Element.dog);

    for (const x of this.collectionX) {

      for (const y of this.collectionY) {

        const grid = {
          X: 0,
          Y: 0,
          item: ''
        };

        grid.X = x;
        grid.Y = y;
        grid.item = '';

        const check = this.settings.elements.find(item =>
          item.gridPosition.positionX === (x - 1) && item.gridPosition.positionY === (y - 1));

        if (check) {
          grid.item = this.GetCellElement(check);
        }

        this.MyGrid.push(grid);
      }
    }

    this.ready = true;
  }

  GetCellElement(position: GridPositionWithElement) {
    switch (position.elementObject) {
      case Element.cat:
        return 'cat';
      case Element.bone:
        return 'bone';
      case Element.dog:
      case Element.formerDog:
        return 'dog' + ' ' + this.GetDogDirection(position);
      default:
        return '';
    }
  }


  GetDogDirection(position: GridPositionWithElement) {
    let val = '';
    switch (position.gridPosition.direction) {
      case Direction.none:
        val = 'none';
        break;
      case Direction.north:
        val = 'up';
        break;
      case Direction.west:
        val = 'left';
        break;
      case Direction.east:
        val = 'right';
        break;
      case Direction.south:
        val = 'down';
        break;
    }
    return val;
  }

  createCollection(max: number, col: number[]) {
    for (let i = 1; i <= max; i++) {
      col.push(i);
    }
    return col;
  }

}


interface Grid {
  X: number;
  Y: number;
  item: string;
}

