import { CellGol } from './CellGol';

export interface GameOfLifeParameters {
    grid: CellGol[];
    dimension: number;
}
