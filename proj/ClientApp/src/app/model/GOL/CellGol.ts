export interface CellGol {
    x: number;
    y: number;
    isAlive: boolean;
    nbAliveCellsAround: number;
}
