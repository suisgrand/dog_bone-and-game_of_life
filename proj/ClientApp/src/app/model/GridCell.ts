export interface GridCell {
    ImageOrPicture: boolean;
    ImageName: string;
}

export enum TextOrPicture {
    Text = 0,
    Image = 1
}
