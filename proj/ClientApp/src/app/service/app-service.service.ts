import { Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClientSettings } from '../GridSettings';
import { Observable, throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';

export const ORIGIN_URL = new InjectionToken<string>('ORIGIN_URL');

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  url: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {

  }

  GetGrid() {
    this.http.get<ClientSettings>(this.baseUrl + 'api/SampleData/GetGrid').pipe(
      map(response => {
        return response;
      })
    );
  }
}
