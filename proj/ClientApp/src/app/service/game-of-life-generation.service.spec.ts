import { TestBed, inject } from '@angular/core/testing';

import { GameOfLifeGenerationService } from './game-of-life-generation.service';

describe('GameOfLifeGenerationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameOfLifeGenerationService]
    });
  });

  it('should be created', inject([GameOfLifeGenerationService], (service: GameOfLifeGenerationService) => {
    expect(service).toBeTruthy();
  }));
});
