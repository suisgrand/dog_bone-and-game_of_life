import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FireServiceService {

  private sub = new Subject<any>();
  public emitter = this.sub.asObservable();

  display(type: string, message: string) {
    this.sub.next({ type, message });
  }

}
