import { TestBed, inject } from '@angular/core/testing';

import { GolServiceService } from './gol-service.service';

describe('GolServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GolServiceService]
    });
  });

  it('should be created', inject([GolServiceService], (service: GolServiceService) => {
    expect(service).toBeTruthy();
  }));
});
