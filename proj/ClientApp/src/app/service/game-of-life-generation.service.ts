import { CellGol } from './../model/GOL/CellGol';
import { Injectable } from '@angular/core';
import { GameOfLifeParameters } from '../model/GOL/GameOfLifeParameters';
import { HomeComponent } from '../home/home.component';

@Injectable({
  providedIn: 'root'
})
export class GameOfLifeGenerationService {

  arrayFate: GameOfLifeParameters = {
    dimension: 0,
    grid: []
  };

  arrayNextGeneration: GameOfLifeParameters = {
    dimension: 0,
    grid: []
  };

  constructor() { }

  public RunSimulation(myParams: GameOfLifeParameters) {

    this.arrayFate = {
      dimension: myParams.dimension,
      grid: JSON.parse(JSON.stringify(myParams.grid))
    };

    this.arrayNextGeneration = {
      dimension: myParams.dimension,
      grid: JSON.parse(JSON.stringify(myParams.grid))
    };

    this.populateNextGeneration();
    myParams.grid = JSON.parse(JSON.stringify(this.arrayNextGeneration.grid));
  }

  private populateNextGeneration() {

    for (let row = 0; row < this.arrayNextGeneration.dimension; row++) {
      for (let col = 0; col < this.arrayNextGeneration.dimension; col++) {

        const fateCell = this.getNextGenerationValue(row, col);
        const inCell = this.arrayNextGeneration.grid.find(item => item.x === row && item.y === col);

        if (fateCell === 1) {
          inCell.isAlive = true;
        } else {
          inCell.isAlive = false;
        }

      }
    }
  }

  private getRow(nRow: number) {
    let rowGrid = 0;
    if (nRow < 0) {
      rowGrid = this.arrayFate.dimension - 1;
    } else if (nRow === this.arrayFate.dimension) {
      rowGrid = 0;
    } else {
      rowGrid = nRow;
    }
    return rowGrid;
  }

  private getColumn(nCol: number) {
    let colGrid = 0;
    if (nCol < 0) {
      colGrid = this.arrayFate.dimension - 1;
    } else if (nCol === this.arrayFate.dimension) {
      colGrid = 0;
    } else {
      colGrid = nCol;
    }
    return colGrid;
  }

  private getNextGenerationValue(row: number, col: number): number {

    let nbrAliveCells = 0;
    let boolAlive = false;
    let retVal = 0;
    const cell = this.arrayFate.grid.find(item => item.x === row && item.y === col);

    if (cell.isAlive === true) {
      boolAlive = true;
    } else {
      boolAlive = false;
    }

    // //calculate the number of alive cells around the :selected cell
    let rowGrid = 0, colGrid = 0;

    for (let nRow = row - 1; nRow < row + 2; nRow++) {
      for (let nCol = col - 1; nCol < col + 2; nCol++) {


        if (!(nRow === row && nCol === col)) {

          rowGrid = this.getRow(nRow);
          colGrid = this.getColumn(nCol);

          const inCell = this.arrayFate.grid.find(item => item.x === rowGrid && item.y === colGrid);

          if (inCell.isAlive) {
            nbrAliveCells++;
          }

        }
      }
    }
    /*now (according to Conway rules):
     * - if the cell is alive/dead,
     * - and based on how many neighboring cells are alive/dead,
     * - the program will decide if the cell will be alive/dead at the next generation
     */


    if (boolAlive === true) {
      if (nbrAliveCells === 2 || nbrAliveCells === 3) {
        retVal = 1;
      } else {
        retVal = 0;
      }
    }  else {
      if (nbrAliveCells === 3) {
        retVal = 1;
      } else {
        retVal = 0;
      }
    }

    return retVal;
  }


}
