import { GameOfLifeParameters } from './../model/GOL/GameOfLifeParameters';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GolServiceService {

  url: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
  }

  GetGrid() {
    this.http.get<GameOfLifeParameters>(this.baseUrl + 'api/GameOfLife').pipe(
      map(response => {
        return response;
      })
    )
  }
}
