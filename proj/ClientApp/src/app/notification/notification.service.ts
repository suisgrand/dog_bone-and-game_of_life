import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() { }

  private sub = new Subject<any>();
  public emmitter = this.sub.asObservable();

  display(type: string, message: string) {
    this.sub.next({});
  }

}
