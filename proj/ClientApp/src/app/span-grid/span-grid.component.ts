import { Component, OnInit, Input, Output } from '@angular/core';
import { GridCell, TextOrPicture } from '../model/GridCell';

@Component({
  selector: ' span-grid',
  templateUrl: './span-grid.component.html',
  styleUrls: ['./span-grid.component.css']
})
export class SpanGridComponent implements OnInit {

  @Input() cellGrid: GridCell

  imagePath = "assets/dog.png";

  constructor() { }

  ngOnInit() {

    //this.cellGrid.Path = "assets/dog.png";
    //console.log("test" + this.cellGrid)
  }

}
