import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpanGridComponent } from './span-grid.component';

describe('SpanGridComponent', () => {
  let component: SpanGridComponent;
  let fixture: ComponentFixture<SpanGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpanGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpanGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
