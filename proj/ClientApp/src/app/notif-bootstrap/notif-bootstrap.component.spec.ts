import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifBootstrapComponent } from './notif-bootstrap.component';

describe('NotifBootstrapComponent', () => {
  let component: NotifBootstrapComponent;
  let fixture: ComponentFixture<NotifBootstrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifBootstrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifBootstrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
