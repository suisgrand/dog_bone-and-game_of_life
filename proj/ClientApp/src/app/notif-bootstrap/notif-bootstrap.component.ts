import { FireServiceService } from './../service/fire-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notif-bootstrap',
  templateUrl: './notif-bootstrap.component.html',
  styleUrls: ['./notif-bootstrap.component.css']
})
export class NotifBootstrapComponent implements OnInit {

  type: string;
  message: string;

  constructor(private fireService: FireServiceService) { }

  ngOnInit() {
    this.fireService.emitter.subscribe(data => {

      this.message = data.message;
      this.type = data.type;
      this.reset();

    });
  }

  reset() {
    setTimeout(() => {
      this.type = null;
      this.message = null;
    }, 5000);
  }

}
