import { GameOfLifeParameters } from './../model/GOL/GameOfLifeParameters';
import { Component, OnInit, Inject, ElementRef, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FireServiceService } from '../service/fire-service.service';
import { GolServiceService } from '../service/gol-service.service';
import { GameOfLifeGenerationService } from '../service/game-of-life-generation.service';

import { interval, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-game-of-life',
  templateUrl: './game-of-life.component.html',
  styleUrls: ['./game-of-life.component.css']
})
export class GameOfLifeComponent implements OnInit, OnDestroy {


  collectionX: number[] = [];
  collectionY: number[] = [];
  golParams: GameOfLifeParameters = {
    dimension: 0,
    grid: [],
  };
  subscription: Subscription;
  disableStart = false;

  constructor(private http: HttpClient,
    private fireService: FireServiceService,
    private gameOfLifeService: GolServiceService,
    private generationService: GameOfLifeGenerationService,
    @Inject('BASE_URL') private baseUrl: string) {

  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit() {

    this.http.get<GameOfLifeParameters>(this.baseUrl + 'api/GameOfLife/GetParameters').subscribe(result => {
      this.golParams = result;
      for (let n = 0; n < this.golParams.dimension; n++) {
        this.collectionX.push(n);
        this.collectionY.push(n);
      }

    });

  }


  StartGame() {
    this.startGameOfLifeGeneration();
    this.disableStart = true;
  }

  StopGame() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.disableStart = false;
  }



  startGameOfLifeGeneration() {

    const source = interval(100);
    const text = 'Your Text Here';
    this.subscription = source.subscribe(() => this.generationService.RunSimulation(this.golParams));

  }

  UserClickCell(event, x, y) {
    const target = event.nativeElement;
    const cell = this.golParams.grid.find(item => item.x === x && item.y === y);

    cell.isAlive = true;
  }

  GetValue(x, y) {
    const val = this.golParams.grid.find(item => item.x === x && item.y === y);
    return val.isAlive === true;
  }

}
