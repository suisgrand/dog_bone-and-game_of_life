import { HomeComponent } from './home/home.component';
import { GolServiceService } from './service/gol-service.service';
import { GameOfLifeComponent } from './game-of-life/game-of-life.component';
import { NotifBootstrapComponent } from './notif-bootstrap/notif-bootstrap.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { DogComponent } from './dog/dog.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { AppServiceService } from './service/app-service.service';
import { NotificationService } from './notification/notification.service';
import { SpanGridComponent } from './span-grid/span-grid.component';
import { FireServiceService } from './service/fire-service.service';
import { GameOfLifeGenerationService } from './service/game-of-life-generation.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    DogComponent,
    CounterComponent,
    FetchDataComponent,
    NotifBootstrapComponent,
    GameOfLifeComponent,
    SpanGridComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'gameoflife', component: GameOfLifeComponent, pathMatch: 'full' },
      { path: 'dogbone', component: DogComponent, pathMatch: 'full' },
      // { path: 'counter', component: CounterComponent },
      // { path: 'fetch-data', component: FetchDataComponent },
    ])
  ],
  providers: [AppServiceService,
    NotificationService,
    GolServiceService,
    GameOfLifeGenerationService,
    FireServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
