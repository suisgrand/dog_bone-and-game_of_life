namespace proj.Dog2Bone
{
    public class Position
    {
        public int positionX { get; set; }
        public int positionY { get; set; }
        public Direction direction { get; set; }
    }
}