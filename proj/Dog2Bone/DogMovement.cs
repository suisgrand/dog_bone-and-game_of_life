using System.Collections.Generic;
using System.Linq;
using proj.Interfaces;

namespace proj.Dog2Bone
{
    public class DogMovement : IDogMovement
    {
        private ClientSettings _settings { get; set; }
        public DogMovement()
        {

        }


        public ClientSettings HandleDogMovement(ClientSettings settings)
        {
            _settings = settings;

            var newDog = _settings.Elements.FirstOrDefault(x => x.ElementObject == Element.Dog);
            var formerDog = _settings.Elements.FirstOrDefault(x => x.ElementObject == Element.FormerDog);

            try
            {
                var directionAndIsCorrect = CheckIfPositionMoveIsCorrect(formerDog, newDog);

                if (directionAndIsCorrect.IsCorrect == true)
                {
                    newDog.GridPosition.direction = directionAndIsCorrect.Direction;
                    newDog.GridPosition.positionX = directionAndIsCorrect.PositionX;
                    newDog.GridPosition.positionY = directionAndIsCorrect.PositionY;
                    // System.Console.WriteLine("x: {0} -- y: {1}", newDog.GridPosition.positionX, newDog.GridPosition.positionY);

                    var dogInCollection = _settings.Elements.FirstOrDefault(x => x.ElementObject == Element.Dog);

                    if (CheckIfDogRanIntoCat(newDog) == true)
                    {
                        _settings.Message = "Dog Ran Into A Cat";
                    }
                    else if (CheckIfDogFoundBone(newDog) == true)
                    {
                        _settings.Message = "Bone Was Found";
                    }
                    else
                    {
                        _settings.Message = "Correct Movement";
                    }
                    _settings.Elements.Remove(formerDog);
                }
                else
                {
                    _settings.Message = "Movement Is Incorrect";
                    ReplaceOldDogPositionWithNewOne(newDog, formerDog);
                    removeFormerDogs();

                }
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.StackTrace);
                throw;
            }

            return _settings;

        }

        private void RemoveNewDog()
        {
            var dog = _settings.Elements.FirstOrDefault(x => x.ElementObject == Element.Dog);
            _settings.Elements.Remove(dog);
        }


        private void removeFormerDogs()
        {
            var formers = _settings.Elements.Where(x => x.ElementObject == Element.FormerDog).ToList();
            if (formers != null)
            {
                foreach (var item in formers)
                {
                    _settings.Elements.Remove(item);
                }
            }

        }


        private void ReplaceOldDogPositionWithNewOne(GridPositionWithElement formerDog, GridPositionWithElement newDog)
        {
            //var pos = _settings.Elements.FirstOrDefault(x => x.ElementObject == Element.Dog);
            if (formerDog.ElementObject == newDog.ElementObject)
            {
                formerDog.GridPosition.positionX = newDog.GridPosition.positionX;
                formerDog.GridPosition.positionY = newDog.GridPosition.positionY;
            }
            else
            {
                formerDog.ElementObject = newDog.ElementObject;
            }
            formerDog.GridPosition.direction = newDog.GridPosition.direction;
            //_settings.Elements.Remove(formerDog);
        }

        private Direction GetNewPosition(GridPositionWithElement dog)
        {
            System.Console.WriteLine("dog.GridPosition.direction " + dog.GridPosition.direction);
            switch (dog.GridPosition.direction)
            {
                case Direction.North:
                    return Direction.East;
                case Direction.South:
                    return Direction.West;
                case Direction.West:
                    return Direction.North;
                case Direction.East:
                    return Direction.South;
                default:
                    return Direction.None;
            }
        }
        private DirectionAndIfTrue CheckIfPositionMoveIsCorrect(GridPositionWithElement formerDog, GridPositionWithElement newDog)
        {
            var isCorrect = false;
            if (newDog.GridPosition.positionY == formerDog.GridPosition.positionY &&
                newDog.GridPosition.positionX == formerDog.GridPosition.positionX)
            {
                return new DirectionAndIfTrue
                {
                    Direction = GetNewPosition(formerDog),
                    IsCorrect = true,
                    PositionX = formerDog.GridPosition.positionX,
                    PositionY = formerDog.GridPosition.positionY
                };
            }

            switch (formerDog.GridPosition.direction)
            {
                case Direction.North:
                    System.Console.WriteLine("new: {0} -- former: {1}", newDog.GridPosition.positionY, formerDog.GridPosition.positionY);
                    if (newDog.GridPosition.positionY == formerDog.GridPosition.positionY - 1 &&
                        newDog.GridPosition.positionX == formerDog.GridPosition.positionX)
                    {
                        isCorrect = true;
                    }

                    break;
                case Direction.East:
                    if (newDog.GridPosition.positionX == formerDog.GridPosition.positionX + 1 &&
                        newDog.GridPosition.positionY == formerDog.GridPosition.positionY)
                    {
                        isCorrect = true;
                    }
                    break;
                case Direction.South:
                    if (newDog.GridPosition.positionY == formerDog.GridPosition.positionY + 1 &&
                        newDog.GridPosition.positionX == formerDog.GridPosition.positionX)
                    {
                        isCorrect = true;
                    }
                    break;
                case Direction.West:
                    if (newDog.GridPosition.positionX == formerDog.GridPosition.positionX - 1 &&
                        newDog.GridPosition.positionY == formerDog.GridPosition.positionY)
                    {
                        isCorrect = true;
                    }
                    break;
                default:
                    isCorrect = false;
                    break;
            }
            return new DirectionAndIfTrue
            {
                Direction = newDog.GridPosition.direction,
                IsCorrect = isCorrect,
                PositionX = newDog.GridPosition.positionX,
                PositionY = newDog.GridPosition.positionY
            };
        }

        private class DirectionAndIfTrue
        {
            public Direction Direction { get; set; }
            public bool IsCorrect { get; set; }
            public int PositionX { get; set; }
            public int PositionY { get; set; }
        }

        public bool CheckIfDogRanIntoCat(GridPositionWithElement newDog)
        {
            var found = this._settings.Elements.Any(x => x.ElementObject == Element.Cat
                && x.GridPosition.positionX == newDog.GridPosition.positionX
                && x.GridPosition.positionY == newDog.GridPosition.positionY);
            return found;
        }

        public bool CheckIfDogFoundBone(GridPositionWithElement newDog)
        {
            var found = this._settings.Elements.Any(x => x.ElementObject == Element.Bone
            && x.GridPosition.positionX == newDog.GridPosition.positionX
                && x.GridPosition.positionY == newDog.GridPosition.positionY);
            return found;
        }

    }
}