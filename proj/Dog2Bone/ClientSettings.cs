using System.Collections.Generic;

namespace proj.Dog2Bone
{
    public class ClientSettings
    {
        public List<GridPositionWithElement> Elements { get; set; }
        public int CellsX { get; set; }
        public int CellsY { get; set; }
        public string Message { get; set; }

        public ClientSettings()
        {
            this.Elements = new List<GridPositionWithElement>();
        }
    }
}