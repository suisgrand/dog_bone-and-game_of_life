namespace proj.Dog2Bone
{
    public static class GlobalSettings
    {
        public const int DIMENSION_X = 5;
        public const int DIMENSION_Y = 4;
        public const int START_POSITION_DOG_X = 0;
        public const int START_POSITION_DOG_Y = 1;
        public const Direction START_DIRECTION_DOG = Direction.North;
        public const int BONE_NUMBER = 1;
    }
}