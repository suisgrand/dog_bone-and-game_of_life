using System;
using System.Collections.Generic;
using System.Linq;
using proj.Interfaces;

namespace proj.Dog2Bone
{
    public class LiveSettings : ILiveSettings
    {
        private int NumberOfCats { get; set; }
        public Position DogPosition { get; set; }
        //public List<Position> Cats { get; set; }
        private List<GridPositionWithElement> UsedPositions { get; set; }

        public LiveSettings()
        {
            LocateElements();
        }

        public ClientSettings GetSettings()
        {
            var settings = new ClientSettings
            {
                CellsX = GlobalSettings.DIMENSION_X,
                CellsY = GlobalSettings.DIMENSION_Y,
                Elements = this.UsedPositions,
                Message = "Game Start"
            };
            return settings;
        }

        public void LocateElements()
        {
            this.UsedPositions = new List<GridPositionWithElement>();
            this.NumberOfCats = SetRandomNumberOfCats();
            LocateDogOnGrid();
            LocateCatsOnGrid();
            LocateBoneOnGrid();
        }

        private int SetRandomNumberOfCats()
        {
            //get grid valuess
            var nbCellsInGrid = GlobalSettings.DIMENSION_X * GlobalSettings.DIMENSION_Y;
            //Max number of cats will be 1/5 of the number of cells minus 1
            var maxNbCats = (nbCellsInGrid / 5) - 1;
            Random rnd = new Random();
            var result = rnd.Next(1, maxNbCats);
            System.Console.WriteLine("number of cats: {0}", result);
            return result;

        }

        private void LocateCatsOnGrid()
        {
            for (var i = 0; i < NumberOfCats; i++)
            {
                var myParams = new ParametersElementLocation
                {
                    X = GetRandomPosition(X_or_Y.x),
                    Y = GetRandomPosition(X_or_Y.y),
                    Direction = Direction.None,
                    Element = Element.Cat
                };
                System.Console.WriteLine("position Cat. X: {0}, Y:{1}", myParams.X, myParams.Y);
                LocateElementOnGrid(myParams);
            }
        }

        private void LocateDogOnGrid()
        {
            var myParams = new ParametersElementLocation
            {
                X = GlobalSettings.START_POSITION_DOG_X,
                Y = GlobalSettings.START_POSITION_DOG_Y,
                Direction = GlobalSettings.START_DIRECTION_DOG,
                Element = Element.Dog
            };
            System.Console.WriteLine("position Cat. X: {0}, Y:{1}", myParams.X, myParams.Y);
            LocateElementOnGrid(myParams);
        }

        private void LocateBoneOnGrid()
        {
            var myParams = new ParametersElementLocation
            {
                X = GetRandomPosition(X_or_Y.x),
                Y = GetRandomPosition(X_or_Y.y),
                Direction = Direction.None,
                Element = Element.Bone
            };
            System.Console.WriteLine("position Bone. X: {0}, Y:{1}", myParams.X, myParams.Y);
            LocateElementOnGrid(myParams);
        }

        private void LocateElementOnGrid(ParametersElementLocation myParams)
        {
            var pos = new Position
            {
                positionX = myParams.X,
                positionY = myParams.Y,
                direction = myParams.Direction
            };
            var myObj = new GridPositionWithElement
            {
                GridPosition = pos,
                ElementObject = myParams.Element
            };

            this.UsedPositions.Add(myObj);
        }

        private int GetRandomPosition(X_or_Y x_or_y)
        {
            Random rnd = new Random();
            var positionIndex = 0;
            var maxNumber = GetMaxNumberOfCellsToConsider(x_or_y);
            if (this.UsedPositions.Count == 0)
            {
                positionIndex = rnd.Next(0, maxNumber);
            }
            else if (this.UsedPositions.Count < maxNumber)
            {
                var found = true;
                while (found == true)
                {
                    positionIndex = rnd.Next(0, maxNumber);
                    if (x_or_y == X_or_Y.x)
                    {
                        found = this.UsedPositions.Any(x => x.GridPosition.positionX == positionIndex);
                    }
                    else
                    {
                        found = this.UsedPositions.Any(x => x.GridPosition.positionY == positionIndex);
                    }
                }
            }
            else
            {
                throw new Exception("The cells defined for the grid are not correct!!");
            }
            return positionIndex;
        }

        private int GetMaxNumberOfCellsToConsider(X_or_Y x_or_y)
        {
            if (x_or_y == X_or_Y.x)
            {
                return GlobalSettings.DIMENSION_X;
            }
            else
            {
                return GlobalSettings.DIMENSION_Y;
            }
        }

        public string GetData()
        {
            return "Hello World";
        }

        private class ParametersElementLocation
        {
            public int X { get; set; }
            public int Y { get; set; }
            public Direction Direction { get; set; }
            public Element Element { get; set; }
        }

        private enum X_or_Y
        {
            x,
            y
        }

    }




}