namespace proj.Dog2Bone
{
    public enum Direction
    {
        North = 0,
        South = 1,
        West = 2,
        East = 3,
        None = 4

    }
}