namespace proj.Dog2Bone
{
    public enum Element
    {
        Dog,
        Cat,
        Bone,
        FormerDog
    }
}