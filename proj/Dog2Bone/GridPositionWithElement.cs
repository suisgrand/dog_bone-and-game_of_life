namespace proj.Dog2Bone
{
    public class GridPositionWithElement
    {
        public Position GridPosition { get; set; }
        public Element ElementObject { get; set; }
    }
}