using proj.Dog2Bone;
namespace proj.Interfaces
{
    public interface IDogMovement
    {
        ClientSettings HandleDogMovement(ClientSettings settings);
    }
}