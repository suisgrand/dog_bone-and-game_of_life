using proj.GameOfLifeModels;

namespace proj.Interfaces.GameOfLife
{
    public interface IGameOfLifeSettings
    {
        GameOfLifeParameters GetDefaultParameters();
    }
}