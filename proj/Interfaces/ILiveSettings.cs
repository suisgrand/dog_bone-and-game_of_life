using proj.Dog2Bone;

namespace proj.Interfaces
{
    public interface ILiveSettings
    {
        string GetData();

        ClientSettings GetSettings();
    }
}