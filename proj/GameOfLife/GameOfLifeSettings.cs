using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using proj.GameOfLifeModels;
using proj.Interfaces.GameOfLife;

namespace proj.GameOfLife
{
    public class GameOfLifeSettings : IGameOfLifeSettings
    {
        private readonly int Dimension;
        private ICollection<CellGOL> Grid;
        public GameOfLifeSettings()
        {
            this.Dimension = 50;
            this.Grid = new Collection<CellGOL>();
        }

        private void GenerateRandomLiveCells()
        {
            Random rnd = new Random();
            var nbMaxLiveCells = Math.Round((this.Dimension * this.Dimension) * 0.5, 0);
            var actualNumberLiveCells = rnd.Next(1, Convert.ToInt32(nbMaxLiveCells));
            var counterLiveCells = 0;

            System.Console.WriteLine("actualNumberLiveCells: {0}, nbMaxLiveCells: {1}");

            var totalX = this.Dimension;
            var totalY = this.Dimension;

            for (var x = 0; x < totalX; x++)
            {
                for (var y = 0; y < totalY; y++)
                {
                    var rndCell = new Random();
                    bool isAlive = false;
                    isAlive = rndCell.Next(3) != 1;
                    counterLiveCells++;
                    // if (counterLiveCells < actualNumberLiveCells)
                    // {
                    //     isAlive = rndCell.Next(3) != 1;
                    //     counterLiveCells++;
                    // }
                    var myCell = new CellGOL { X = x, Y = y, IsAlive = isAlive };
                    this.Grid.Add(myCell);
                }
            }
        }
        public GameOfLifeParameters GetDefaultParameters()
        {
            var myParams = new GameOfLifeParameters();
            myParams.Dimension = this.Dimension;
            GenerateRandomLiveCells();
            myParams.Grid = this.Grid;

            return myParams;
        }

    }
}